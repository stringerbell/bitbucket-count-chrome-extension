chrome.extension.sendMessage({}, function(response) {
  const readyStateCheckInterval = setInterval(function() {
    if (document.readyState === 'complete') {
      clearInterval(readyStateCheckInterval);
      // get scripts with nonce attribute
      const nonceScripts = document.querySelectorAll('script[nonce]');
      // pull out giant React JSON blob
      const json = JSON.parse(
        Object.getOwnPropertyNames(nonceScripts)
          .map((script) => {
            if (
              nonceScripts[script].text.includes('window.__initial_state__')
            ) {
              return nonceScripts[script].text;
            }
          })
          .filter((item) => item !== undefined) // get rid of empty results
          .pop()
          // getting rid of other React JS things
          .split('window.__initial_state__ = ')
          .pop()
          .split('window.__settings__ = ')
          .shift()
          .replace(';', ''),
      );

      const endpoint =
        json.section.repository.currentRepository.links.self.href;
      fetch(endpoint + '/pullrequests', {
        credentials: 'same-origin',
      })
        .then(function(response) {
          return response.json();
        })
        .then(function(json) {
          // find the navbar pull requests, and set the text from above JSON data
          const requests = json.size === 1 ? 'Request' : 'Requests';
          jQuery('#adg3-navigation')
            .find("div :contains('Pull requests')")
            .filter((idx, div) => {
              return div.innerText === 'Pull requests\n';
            })
            .last()
            .text((json.size || 0) + ` Open Pull ${requests}`);
        });
    }
  }, 10);
});
