How to install:

- clone this repository
- Go to chrome extensions (chrome://extensions/)
- Check the "Developer Mode" checkbox
- Click on "Load unpacked extension" button.
- From there choose the extension/chrome/src folder of this repository.
![result](result.png)
